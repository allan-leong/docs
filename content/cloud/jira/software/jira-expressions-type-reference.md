---
title: "Jira expressions Type Reference"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2019-06-24"
---
{{% include path="docs/content/cloud/connect/concepts/jira-expressions-type-reference.snippet.md" %}}