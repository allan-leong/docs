---
title: "Atlassian Connect app migration guide to improve user privacy"
platform: cloud
product: jswcloud
category: devguide
subcategory: learning
guides: guides
date: "2018-09-28"
aliases:
- /cloud/jira/software/connect-app-migration-guide.html
- /cloud/jira/software/connect-app-migration-guide.md
---
{{% include path="docs/content/cloud/connect/guides/connect-app-gdpr-migrations-header.snippet.md" %}}

{{% note title="Some changes are still in development" %}}
We always strive to give our developers as much time as possible to plan and do the required work. This is why we have published this migration guide before all changes have been release into production.
<br><br>
See [APIs affected by GDPR API migration](../api-changes-for-user-privacy-announcement-connect/) for the detailed list and implementation status of API changes.
<br><br>
*We do not advise enabling the Connect GDPR API migration for existing Marketplace-listed production versions. We will continue to add API changes under the API migration opt-in, so apps may break. Check this page to find out when it is safe to enable the API migration for your customers.*
<br><br>
Need help? Head to the [Jira Cloud](https://community.developer.atlassian.com/t/jira-cloud-rest-api-privacy-updates-and-migration-guide/24166) forum on the developer community and start a thread.
{{% /note %}}

{{% include path="docs/content/cloud/connect/guides/connect-app-gdpr-migrations.snippet.md" %}}
