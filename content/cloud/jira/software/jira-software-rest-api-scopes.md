---
title: "Jira Software REST API scopes"
platform: cloud
product: jswcloud
category: reference
subcategory: api
date: "2017-09-11"
type: scopes
scopeskey: jiraSoftware
---
# Jira Software REST API scopes

{{% include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" %}}