---
title: "Jira Software Context Parameters"
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-software-context-parameters-39990789.html
- /jiracloud/jira-software-context-parameters-39990789.md
confluence_id: 39990789
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990789
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990789
date: "2019-03-14"
---

# Jira Software context parameters

Context parameters are special, context-specific values that can be supplied to your apps. 
These values are provided via special tokens, which are inserted into the URL properties of 
your [Jira Software modules], specifically panels and actions.

Using context parameters, your Atlassian Connect apps can selectively alter their behavior 
based on information provided by Jira Software. Common examples include displaying alternate 
content, or even performing entirely different operations based on the available context.

## Using parameters

Context parameters can be used with any UI module that takes a URL property, as specified in your 
`atlassian-connect.json` descriptor. To use a context parameter, simply insert the corresponding 
key name, surrounded by curly-braces, at any location within your URL. The parameter will then be 
substituted into the URL as Jira Software renders your UI module.

For example, the URL in the following snippet includes the `board.id` and `board.type` parameters:

``` java
...
"modules": {      
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 1
        }
    ]
}
...
```

## Jira Software parameters

The following table details the list of context parameters provided by Jira Software:

<table>
    <thead>
        <tr>
            <th>Parameter key</th>
            <th>Description</th>
            <th>Available for</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>board.id</code></td>
            <td>The ID of the current board.</td>
            <td></td>
        </tr>
        <tr>
            <td><code>board.type</code></td>
            <td>The type (such as <em>scrum</em> or <em>kanban</em>) of the current request type.</td>
            <td></td>
        </tr>
        <tr>
            <td><code>board.screen</code></td>
            <td>The screen of the current board.<br>
                <em>Note that the <code>board.mode</code> parameter was deprecated in favor of this parameter.</em></td>
            <td><p>Modules that are displayed in multiple board screens.</p></td>
        </tr>
        <tr>
            <td><code>sprint.id</code></td>
            <td>The ID of the current sprint.</td>
            <td></td>
        </tr>
        <tr>
            <td><code>sprint.state</code></td>
            <td>The state of the current sprint.</td>
            <td></td>
        </tr>
    </tbody>
</table>

## Additional parameters

The Jira platform provides a number of additional context parameters that Jira Software apps can use. For a full 
list of these context parameters, see the [JIRA Platform Cloud documentation](/cloud/jira/platform/context-parameters/).

[Jira Software modules]: /cloud/jira/software/about-jira-modules/