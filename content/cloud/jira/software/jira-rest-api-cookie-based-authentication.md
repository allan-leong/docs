---
title: "Cookie-based auth for REST APIs"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2017-08-25"
---
{{% reuse-page path="docs/content/cloud/jira/platform/jira-rest-api-cookie-based-authentication.md" %}}