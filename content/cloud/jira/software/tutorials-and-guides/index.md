---
title: Guides and tutorials
product: jswcloud
category: devguide
layout: tutorials-and-guides
date: "2019-04-24"
---

# Tutorials and guides

## Tutorials

[Adding a board configuration page](/cloud/jira/software/adding-a-board-configuration-page/)

[Adding a dropdown to an Agile board](/cloud/jira/software/adding-a-dropdown-to-an-agile-board/)

## Guides

[Atlassian Connect app migration guide to improve user privacy](/cloud/jira/software/connect-app-migration-guide/)

[Build a Jira app using a framework](/cloud/jira/software/build-a-jira-app-using-a-framework/)
