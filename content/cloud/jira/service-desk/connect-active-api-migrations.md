---
title: "Connect API migration"
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
date: "2019-02-25"
aliases:
- /cloud/jira/service-desk/connect-active-api-migrations.html
- /cloud/jira/service-desk/connect-active-api-migrations.md
---
{{% include path="docs/content/cloud/connect/reference/active-api-migrations.snippet.md" %}}