---
title: "Internationalization"
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
date: "2017-08-25"
---
{{% include path="docs/content/cloud/connect/concepts/internationalization.snippet.md" %}}