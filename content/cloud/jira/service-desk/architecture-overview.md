---
title: Architecture overview
platform: cloud
product: jsdcloud
category: devguide
subcategory: intro
date: "2017-08-24"
---
{{% include path="docs/content/cloud/connect/concepts/cloud-development.snippet.md" %}}
