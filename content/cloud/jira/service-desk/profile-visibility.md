---
title: "Profile visibility"
platform: cloud
product: jsdcloud
category: devguide
subcategory: privacy
date: "2019-05-20"
---

{{% include path="docs/content/cloud/connect/guides/profile-visibility.snippet.md" %}}
