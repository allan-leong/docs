---
title: "Project settings UI locations"
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-project-configuration-39988366.html
- /jiracloud/jira-platform-modules-project-configuration-39988366.md
confluence_id: 39988366
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988366
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988366
date: "2017-09-11"
---
# Project settings UI locations

If your app allows for per-project configuration options, you can use a [Project Admin Tab Panel] within that project's *Settings* section for users to configure your app in the context of that project. The project settings section supports web sections for your app to define one or more links (such as the location for a Project Admin Tab Panel).

## Available locations

For web sections:

* `atl.jira.proj.config`

##### In classic projects

![Jira project settings location classic](../images/jira-project-settings-locations.png)

##### In next-gen projects

![Jira project settings location next gen](../images/jira-project-settings-locations-next-gen.png)

### Sample descriptor JSON
``` json
...
"modules": {
    "webSections": [
      {
        "key": "example-menu-section",
        "location": "atl.jira.proj.config",
        "name": {
          "value": "Project admin app"
        }
      }
    ],
    "jiraProjectAdminTabPanels": [
      {
        "location": "example-menu-section",
        "url": "/admin",
        "weight": 100,
        "name": {
          "value": "App page"
        },
        "key": "admin-panel"
      }
    ]
...
```

### Properties

The properties required for this location are the standard ones defined in the documentation for [web sections].

----

## Project summary area

Adds panels to the project summary area.

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title">
        <strong>Deprecated</strong>
    </p>
    <p>
    We expect to remove the project summary area in Jira Cloud in the near future, so we recommend that you don't add new project summary panels. Instead, you should provide a <a href="/cloud/jira/platform/modules/project-admin-tab-panel/">Project Admin Tab Panel</a> if you need to display content in Jira's project settings UI.
    </p>
</div>

#### Module type
`webPanel`

#### Screenshot
<img src="/cloud/jira/platform/images/jira-projectconfig-summary.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-panel",
            "location": "webpanels.admin.summary.left-panels",
            "name": {
                "value": "Example panel"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location to `webpanels.admin.summary.left-panels` or `webpanels.admin.summary.right-panels`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

  [i18n property]: /cloud/jira/platform/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters

[web sections]: /cloud/jira/platform/modules/web-section
[web items]: /cloud/jira/platform/modules/web-item/
[Project Admin Tab Panel]: /cloud/jira/platform/modules/project-admin-tab-panel/