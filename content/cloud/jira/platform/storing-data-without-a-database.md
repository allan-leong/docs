---
title: Storing data without a database
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2018-06-12"
aliases:
- /cloud/jira/platform/storing-data-without-a-database.html
- /cloud/jira/platform/storing-data-without-a-database.md
---
{{% include path="docs/content/cloud/connect/concepts/storing-jira-data-without-a-database.snippet.md" %}}