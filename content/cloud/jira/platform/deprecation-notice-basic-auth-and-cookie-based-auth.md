---
title: "Deprecation notice - Basic authentication with passwords and cookie-based authentication"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-03-01"
---

# Deprecation notice - Basic authentication with passwords and cookie-based authentication

<div class="aui-message note">
    <div class="icon"></div>
    <p>
        <strong>The deprecation period for this functionality has ended. From June 3rd, 2019, we will be progressively disabling the usage of this authentication method. </strong>
    </p>
    </br>
    <p>If you are using a REST endpoint in Jira with basic authentication, update your app or integration to use API tokens, OAuth, or Atlassian Connect.
    </p>
    <br/>
</div>

Atlassian has introduced support for [API tokens] for all Atlassian Cloud sites as a replacement 
for [basic authentication] requests that previously used a password or primary credential for an Atlassian 
account, as well as [cookie-based authentication].

Basic authentication with passwords and cookie-based authentication are now deprecated and will be removed in 2019 in 
accordance with the [Atlassian REST API policy].

## What is changing?

If you currently rely on a script, integration, or application that makes requests to Jira Cloud with basic or cookie-based 
authentication, you should update it to use basic authentication with an API token, OAuth, or Atlassian Connect as soon as 
possible.

[Learn more about creating API tokens].

## Why is Atlassian making this change?

Using primary account credentials like passwords outside of Atlassian's secure login screens creates security risks for our 
customers. API tokens allow users to generate unique one-off tokens for each app or integration that can be easily revoked if 
needed.

Furthermore, as customers adopt features like two-factor authentication and SAML, passwords may not work in all cases. API tokens have been specifically designed as an alternative for Atlassian accounts in organizations that are using these features.

## Which APIs and methods will be restricted?

For the following APIs and pages, all requests using basic authentication with a non-API token credential will return 401 (
Unauthorized) after the deprecation period:

* Jira Cloud public REST API
* Jira Software public REST API
* Jira Service Desk public REST API
* All Jira Cloud web pages

The following Jira Cloud REST API endpoint will be removed:

* [`/rest/auth/1/session`](/cloud/jira/platform/rest/#api-auth-1-session-get)

[API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
[basic authentication]: /cloud/jira/platform/jira-rest-api-basic-authentication
[cookie-based authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication/
[Atlassian REST API policy]: /platform/marketplace/atlassian-rest-api-policy/
[Learn more about creating API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html