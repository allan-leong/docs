---
title: "Deprecation notice: Updating project types using the REST API" 
platform: cloud 
product: confcloud 
category: devguide 
subcategory: changelog 
date: "2019-06-03"
---

# Deprecation notice: Updating project types using the REST API

{{% note %}} The six-month deprecation period for these changes began on 03 June
2019.

Support for these APIs will be removed on 03 December 2019. {{% /note %}}

We are deprecating the ability to change project type in Jira Cloud via REST
APIs.

## What are project types?

As part of the introduction of Jira Cloud products, the concept of project type
was introduced. Each product (Jira Core, Jira Service Desk, or Jira Software)
delivers a tailored experience to its users, and has an associated project type
that in turn offers product-specific features.

[Read more about these project
types](https://confluence.atlassian.com/x/GwiiLQ).

## What changed and why?

We are no longer supporting the ability to change project type from both Jira
Cloud UI and Jira Cloud REST APIs. This change is happening because switching
between the project types left customers with an unusable project, hence a bad
experience.

Here are the changes that have happened:

### Jira Cloud UI

- If you have a Jira Classic project, you could have changed the project type
  from *Project settings*. This is now disabled.

### Jira Cloud REST API

- [Update project
  API](/cloud/jira/platform/rest/v3/#api-rest-api-3-project-projectIdOrKey-put)
  won’t support `projectTypeKey` parameter.
- [Update project type
  API](/cloud/jira/platform/rest/v3/#api-rest-api-3-project-projectIdOrKey-type-newProjectTypeKey-put)
  is deprecated.

## What are the alternatives?

To change project type, create a new project and bulk move your issues into it.
[Read our step-by-step
instructions](https://confluence.atlassian.com/x/yEKeOQ).

## Need help

If you need help with this change, ask in the [Jira Cloud
development](https://community.developer.atlassian.com/c/Jira/jira-cloud) forum
in the [Developer Community](https://community.developer.atlassian.com/).