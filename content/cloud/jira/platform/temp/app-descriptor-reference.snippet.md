## App descriptor structure

``` json
{
  "modules": {},
  "key": "my-app-key",
  "name": "My Connect App",
  "description": "A connect app that does something",
  "vendor": {
    "name": "My Company",
    "url": "http://www.example.com"
  },
  "links": {
    "self": "http://www.example.com/connect/jira/atlassian-connect.json"
  },
  "lifecycle": {
    "installed": "/installed",
    "uninstalled": "/uninstalled"
  },
  "baseUrl": "http://www.example.com/connect/jira",
  "authentication": {
    "type": "jwt"
  },
  "enableLicensing": true,
  "scopes": [
    "read",
    "write"
  ]
}
```

<table summary="App descriptor properties">
  <tbody>
    <tr>
      <td><code>authentication</code></td>
      <td><p><strong>Type:</strong> <code>Authentication</code></p>
        <p><strong>Required:</strong> <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span></p>
        <p><strong>Description:</strong> Defines the authentication type to use when signing requests between the host application and the connect app.</p>
        </td>
    </tr>
    <tr>
      <td><code>baseUrl</code></td>
      <td><p><strong>Type:</strong> <code>string</code> (<code>uri</code>)</p>
        <p><strong>Required:</strong> <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span></p>
        <p><strong>Description:</strong> The base url of the remote app, which is used for all communications back to the app instance. Once the app is installed in a product, the app's baseUrl cannot be changed without first uninstalling the app. This is important; choose your baseUrl wisely before making your app public.
        <p>The baseUrl must start with <code>https://</code> to ensure that all data is sent securely between our cloud instances and your app.
        </p>
        <p>Note: each app must have a unique baseUrl. If you would like to serve multiple apps from the same host, consider adding a path prefix into the baseUrl.
        </p></p>
      </td>
    </tr>
    <tr>
      <td><code>key</code></td>
      <td><p><strong>Type:</strong> <code>string</code> (<code>^[a-zA-Z0-9-._]+$</code>)</p>
        <p><strong>Required:</strong> <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span></p>
        <p><strong>Description:</strong> A unique key to identify the app.This key must be &lt;= 64 characters.</p>
      </td>
    </tr>
    <tr>
      <td><code>apiVersion</code></td>
      <td><p><strong>Type:</strong> <code>integer</code></p>
        <p><strong>Description:</strong> The API version is an OPTIONAL integer. If omitted we will infer an API version of 1.</p>
      <p>The intention behind the API version is to allow vendors the ability to beta test a major revision to their Connect app as a private version, and have a seamless transition for those beta customers (and existing customers) once the major revision is launched. </p>
      <p>Vendors can accomplish this by listing a new private version of their app, with a new descriptor hosted at a new URL. </p>
      <p>They use the Atlassian Marketplace's access token facilities to share this version with customers (or for internal use). 
        When this version is ready to be taken live, it can be transitioned from private to public, and all customers will be seamlessly updated.</p>
      <p>It's important to note that this approach allows vendors to create new versions manually, despite the fact that in the common case, the versions are automatically created. This has a few benefits-- for example, it gives vendors the ability to change their descriptor URL if they need to (the descriptor URL will be immutable for existing versions)
      </p>
      </td>
    </tr>
    <tr>
      <td><code>description</code></td>
      <td><p><strong>Type:</strong> <code>string</code></p>
        <p><strong>Description:</strong> A human readable description of what the app does. The description will be visible in the <strong>Manage Apps</strong> section of the administration console. Provide meaningful and identifying information for the instance administrator.
      </p>
      </td>
    </tr>
    <tr>
      <td><code>enableLicensing</code></td>
      <td><p><strong>Type:</strong> <code>boolean</code></p>
        <p><strong>Defaults to:</strong> <code>false</code></p>
        <p><strong>Description:</strong> Whether or not to enable licensing options in the UPM/Marketplace for this app.</p>
      </td>
    </tr>
    <tr>
      <td><code>lifecycle</code></td>
      <td><p><strong>Type:</strong> <code>Lifecycle</code></p>
        <p><strong>Description:</strong> Allows the app to register for app lifecycle notifications.</p>
      </td>
    </tr>
    <tr>
      <td><code>links</code></td>
      <td><p><strong>Type:</strong> <code>object</code></p>
        <p><strong>Description:</strong> A set of links that the app wishes to publish
        <code data-format="block" data-language="json">{
  "links": {
    "self": "https://app.domain.com/atlassian-connect.json",
    "documentation": "https://app.domain.com/docs"
  }
}</code>
      </p></td>
    </tr>
    <tr>
      <td><code>modules</code></td>
      <td><p><strong>Type:</strong> <code>object</code></p>
        <p><strong>Description:</strong> The list of modules this app provides.</p>
      </td>
    </tr>
    <tr>
      <td><code>name</code></td>
      <td><p><strong>Type:</strong> <code>string</code></p>
        <p><strong>Description:</strong> The human-readable name of the app.</p>
      </td>
    </tr>
    <tr>
      <td><code>scopes</code></td>
      <td><p><strong>Type:</strong> [<code>string</code>, … ]</p>
        <p><strong>Description:</strong> Set of <a href="https://developer.atlassian.com/cloud/jira/platform/scopes/">scopes</a> requested by this app.
        <code data-format="block" data-language="json">{
  "scopes": [
  "read",
  "write"
  ]
}</code></p>
      </td>
    </tr>
    <tr>
      <td><code>vendor</code></td>
      <td><p><strong>Type:</strong> <code>App Vendor</code></p>
        <p><strong>Description:</strong> The vendor who is offering the app.</p>
      </td>
    </tr>
  </tbody>
</table>