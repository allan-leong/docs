## Authentication

Defines the authentication type to use when signing requests from the host application to the Connect app.

### Example

``` json
{
  "authentication": {
    "type": "jwt"
  }
}
```

## Properties

<table summary="Authentication properties">
  <tbody>
    <tr>
      <td><code>publicKey</code></td>
      <td><p><strong>Type:</strong> <code>string</code></p>
        <p><strong>Description:</strong> The public key used for asymmetric key encryption. Ignored when using JWT with a shared secret.</p>
        </td>
    </tr>
    <tr>
      <td><code>type</code></td>
      <td><p><strong>Type:</strong> <code>string</code></p>
      	<p><strong>Defaults to:</strong> <code>jwt</code></p>
      	<p><strong>Allowed values:</strong>
      		<li><code>jwt</code></li>
      		<li><code>JWT</code></li>
      		<li><code>none</code></li>
      		<li><code>NONE</code></li></p>
        <p><strong>Description:</strong> The type of authentication to use.</p>
      </td>
    </tr>
  </tbody>
</table>