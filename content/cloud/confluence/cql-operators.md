---
title: "CQL operators"
platform: cloud
product: confcloud
category: reference
subcategory: cql
date: "2016-11-17"
aliases:
- confcloud/cql-operators-reference-39985874.html
- /confcloud/cql-operators-reference-39985874.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985874
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985874
confluence_id: 39985874
---
# CQL operators

An operator in CQL is one or more symbols or words which compares the value of a [field](/cloud/confluence/cql-fields) on its left with one or more values (or [functions](/cloud/confluence/cql-functions)) on its right, such that only true results are retrieved by the clause. Some operators may use the `NOT` keyword.

## EQUALS

The "`=`" operator is used to search for content where the value of the specified field exactly matches the specified value (cannot be used with [text](/cloud/confluence/advanced-searching-using-cql#text) fields; see the [CONTAINS](#contains) operator instead.)

To find content where the value of a specified field exactly matches *multiple* values, use multiple "`=`" statements with the [AND](/cloud/confluence/cql-keywords#and) operator.

### Examples

-   Find all content that was created by the user with the accountId `99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e`:

    ``` bash
    creator = "99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e"
    ```

-   Find all content that has the title "Advanced Searching"

    ``` bash
    title = "Advanced Searching"
    ```

## NOT EQUALS

The "`!=`" operator is used to search for content where the value of the specified field does not match the specified value. It cannot be used with [text](/cloud/confluence/advanced-searching-using-cql#text) fields; see the [DOES NOT CONTAIN](#does-not-contain) ("`!~`") operator instead.

{{% note %}}Typing `field != value` is the same as typing `NOT field = value`.{{% /note %}}

Currently a negative expression cannot be the first clause in a CQL statement

### Examples

-   Find all content in the DEV space that was created by someone other than the user with the accountId `99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e`:

    ``` bash
    space = DEV and not creator = "99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e"
    ```

    or:

    ``` bash
    space = DEV and creator != "99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e"
    ```

-   Find all content that was created by me but doesn't mention me

    ``` bash
    creator = currentUser() and mention != currentUser()
    ```

## GREATER THAN

The "`>`" operator is used to search for content where the value of the specified field is greater than the specified value. Cannot be used with [text] fields.

The "`>`" operator can only be used with fields which support range operators (e.g. date fields and numeric fields). To see a field's supported operators, check the individual [field](/cloud/confluence/advanced-searching-using-cql#field-reference) reference.

### Examples

-   Find all content created in the last 4 weeks

    ``` bash
    created > now("-4w")
    ```

-   Find all attachments last modified since the start of the month

    ``` bash
    created > startOfMonth() and type = attachment
    ```

## GREATER THAN EQUALS

The "`>=`" operator is used to search for content where the value of the specified field is greater than or equal to the specified value. Cannot be used with [text](/cloud/confluence/advanced-searching-using-cql#text) fields.

The "`>=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/cloud/confluence/advanced-searching-using-cql#field-reference) reference.

### Examples

-   Find all content created on or after 31/12/2008:

    ``` bash
    created >= "2008/12/31"
    ```

## LESS THAN

The "`<`" operator is used to search for content where the value of the specified field is less than the specified value. Cannot be used with [text](/cloud/confluence/advanced-searching-using-cql#text) fields.

The "`<`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/cloud/confluence/advanced-searching-using-cql#field-reference) reference.

### Examples

-   Find all pages lastModified before the start of the year

    ``` bash
    lastModified < startOfYear() and type = page
    ```

## LESS THAN EQUALS

The "`<=`" operator is used to search for content where the value of the specified field is less than or equal to than the specified value. Cannot be used with [text](/cloud/confluence/advanced-searching-using-cql#text) fields.

The "`<=`" operator can only be used with fields which support range operators (e.g. date fields). To see a field's supported operators, check the individual [field](/cloud/confluence/advanced-searching-using-cql#field-reference) reference.

### Examples

-   Find blogposts created in the since the start of the fortnight

    ``` bash
    created >= startOfWeek("-1w") and type = blogpost
    ```

## IN

The "`IN`" operator is used to search for content where the value of the specified field is one of multiple specified values. The values are specified as a comma-delimited list, surrounded by parentheses.

Using "`IN`" is equivalent to using multiple `EQUALS (=)` statements with the OR keyword, but is shorter and more convenient. That is, typing `creator IN ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01", "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7")` is the same as typing `creator = "99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e" OR creator = "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01" OR creator = "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7"`.

### Examples

Find all content that mentions any of the users with the accountIds `99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e`,`48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01`, or `2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7`

    ``` bash
    mention in ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01", "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7")
    ```

-   Find all content where the creator or contributor is either the user with the accountId `99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e` or the user with the accountId `48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01`

    ``` bash
    creator in ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01") or contributor in ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01")
    ```

## NOT IN

The "`NOT IN`" operator is used to search for content where the value of the specified field is not one of multiple specified values.

Using "`NOT IN`" is equivalent to using multiple `NOT_EQUALS (!=)` statements, but is shorter and more convenient. That is, typing `creator NOT IN ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01", "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7")` is the same as typing `creator != "99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e" AND creator != "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01" AND creator != "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7"`.

### Examples

-   Find all content where the creator is someone other than the users with the accountIds `99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e`,`48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01`, or `2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7`:

    ``` bash
    space = DEV and creator not in ("99:27935d01-XXXX-XXXX-XXXX-a9b8d3b2ae2e", "48293:5s04-XXXX-XXXX-XXXX-d7a9b9d8c9f01", "2223:48d-3a-XXXX-XXXX-XXXX-8d9dd0e98as7")
    ```

## CONTAINS

The "`~`" operator is used to search for content where the value of the specified field matches the specified value (either an exact match or a "fuzzy" match -- see examples below). The "~" operator can only be used with text fields, for example:

-   title
-   text

{{% note %}}When using the "`~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](/cloud/confluence/performing-text-searches-using-cql).{{% /note %}}

### Examples

-   Find content created by users with a full name (depending on profile visibility) like *alana*

    ``` bash
    creator.fullname ~ "alana"
    ```

-   Find all content where the title contains the word "win" (or simple derivatives of that word, such as "wins"):

    ``` bash
    title ~ win
    ```

-   Find all content where the title contains a wild-card match for the word "win":

    ``` bash
    title ~ "win*"
    ```

-   Find all content where the text contains the word "advanced" and the word "search":

    ``` bash
    text ~ "advanced search"
    ```

## DOES NOT CONTAIN

The "`!~`" operator is used to search for content where the value of the specified field is not a "fuzzy" match for the specified value. The "!~" operator can only be used with text fields, for example:

-   title
-   text

{{% note %}}When using the "`!~`" operator, the value on the right-hand side of the operator can be specified using [Confluence text-search syntax](/cloud/confluence/performing-text-searches-using-cql).{{% /note %}}

### Examples

-   Find all content where the title does not contain the word "run" (or derivatives of that word, such as "running" or "ran"):

    ``` bash
    space = DEV and title !~ run
    ```