---
title: "Tutorials and guides"
product: confcloud
category: devguide
layout: tutorials-and-guides
date: "2019-04-24"
---

# Tutorials and guides

## Tutorials

[Introduction to Confluence Connect](/cloud/confluence/introduction-to-confluence-connect/)

[Lesson 1 - The source awakens](/cloud/confluence/lesson-1-the-source-awakens/)

[Lesson 2 - Rise of the macros](/cloud/confluence/lesson-2-rise-of-the-macros/)

[Content byline items](/cloud/confluence/content-byline-items-with-confluence-connect/)

[Lesson 1 - Introduction to the byline](/cloud/confluence/lesson-1-introduction-to-the-byline/)

[Lesson 2 - Dynamically update your byline](/cloud/confluence/lesson-2-dynamically-update-your-byline/)

[Custom content with Confluence Connect](/cloud/confluence/custom-content-with-confluence-connect/)

[Lesson 1 - A new content type](/cloud/confluence/lesson-1-a-new-content-type/)

[Lesson 2 - Adding content - Customers, ahoy!](/cloud/confluence/lesson-2-adding-content-customers-ahoy/)

[Lesson 3 - Extra searching capabilities](/cloud/confluence/lesson-3-extra-searching-capabilities/)

[Lesson 4 - Noting down information](/cloud/confluence/lesson-4-noting-down-information/)

[Multi-page blueprints with Confluence Connect](/cloud/confluence/multi-page-blueprints-with-confluence-connect/)

[Create a Confluence theme](/cloud/confluence/create-a-confluence-theme/)

## Guides

[Atlassian Connect App migration guide to improve user privacy](/cloud/confluence/connect-app-migration-guide/)

[Macro autoconvert with Confluence Connect](/cloud/confluence/macro-autoconvert-with-confluence-connect/)

[Macro custom editor with Confluence Connect](/cloud/confluence/macro-custom-editor-with-confluence-connect/)
