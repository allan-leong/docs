---
title: "Internationalization"
platform: cloud
product: confcloud
category: devguide
subcategory: blocks
date: "2017-07-26"
---
{{% include path="docs/content/cloud/connect/concepts/internationalization.snippet.md" %}}