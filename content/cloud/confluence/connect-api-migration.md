---
title: Connect API migration
platform: cloud
product: confcloud
category: devguide
subcategory: learning
date: "2018-09-01"
aliases:
- /cloud/confluence/connect-api-migration.html
- /cloud/confluence/connect-api-migration.md
---
{{% include path="docs/content/cloud/connect/concepts/connect-api-migration.snippet.md" %}}
