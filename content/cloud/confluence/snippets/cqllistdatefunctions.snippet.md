
-   [endOfDay()](/cloud/confluence/cql-functions#endofday)
-   [endOfMonth()](/cloud/confluence/cql-functions#endofmonth)
-   [endOfWeek()](/cloud/confluence/cql-functions#endofweek)
-   [endOfYear()](/cloud/confluence/cql-functions#endofyear)
-   [startOfDay()](/cloud/confluence/cql-functions#startofday)
-   [startOfMonth()](/cloud/confluence/cql-functions#startofmonth)
-   [startOfWeek()](/cloud/confluence/cql-functions#startofweek)
-   [startOfYear()](/cloud/confluence/cql-functions#startofyear)




