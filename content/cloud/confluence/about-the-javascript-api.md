---
title: About the JavaScript API
platform: cloud
product: confcloud
category: reference
subcategory: jsapi
date: "2016-11-01"
---
{{% include path="docs/content/cloud/connect/concepts/javascript-api.snippet.md" %}}