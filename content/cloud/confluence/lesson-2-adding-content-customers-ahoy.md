---
title: "Lesson 2 - Adding content - Customers, ahoy!"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2019-01-23"
aliases:
- confcloud/lesson-2-adding-content-customers-ahoy-40511959.html
- /confcloud/lesson-2-adding-content-customers-ahoy-40511959.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40511959
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40511959
confluence_id: 40511959
---

# Lesson 2 - Adding content - Customers, ahoy!

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Description</strong></td>
<td>Setup and create instances of a new Customer content type.</td>
</tr>
<tr class="even">
<td><strong>Level</strong></td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>Estimated time</strong></td>
<td>15 minutes</td>
</tr>
<tr class="even">
<td><strong>Example</strong></td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/custom-content/" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/custom-content/</a></td>
</tr>
</tbody>
</table>

## Prerequisites

Ensure you have worked through [Lesson 1 - A New Content Type](/cloud/confluence/lesson-1-a-new-content-type) in the Custom content series.

## The missing link

Okay, so now that we have our Customer content type, and generalPages which will soon visualize this content, how do we form the link between the Content API and our UI? Let's quickly understand this before jumping into creating some instances of our content type. To acquire the data we need, we can use one of two approaches (or even both): 

-   **The Backendy Approach: **
    With this approach, you can do a server-side call to the Confluence REST API, and collect all content of the type you are interested in. Typically, this will look something like this:

    ``` javascript
    //  Setup a HTTP client with all under-the-hood authentication and validation handled by ACE.
    var httpClient = addon.httpClient(req);
    httpClient.get({
        url: '/rest/api/content',
        qs: {
          type: 'ac:my-app:customer',
          space: {
            key: '<your-space-key>'
          }
        },
    }, function(err, res, body){
        // Handle response data here...
    });
    ```

    This method provides you the ability to do any server-side data manipulation, by leveraging any custom architecture or services you may be using in your backend.

-   **The Frontendy Approach**
    But, it's understandable that you may not need all this. If, instead of a backend service, you intend on sending this API data through a custom front-end stack (for example using React, Webpack, ES6), you can easily use the [Connect Request JS API](/cloud/confluence/jsapi/request/) to make authenticated calls to Confluence. This would typically look like this: 

    ``` javascript
    AP.require('request', function(request){
      request({
        url: '/rest/api/content',
        data: {
            type: 'ac:my-app:customer',
            space: {
                key: '<your-space-key>'
            }
        },
        success: (data) => {
            console.log(data);
        },
        error: (err) => {
            console.log(err);   
        }
      });
    });
    ```

We can use the same backend or frontend protocol for all HTTP requests within our app service. 

## Displaying all Customers

In previous lesson, we created a generalPage for this with key `customerList` that routes to **/list-customers**. Let's implement that page now:

1. Firstly, we add **/list-customers** to our routes:

    **routes/index.js**
    ```javascript
    app.get('/list-customers', addon.authenticate(), function (req, res) {
            var spaceKey =  req.query['spaceKey']
            res.render('list-customers', {
                spaceKey: spaceKey
            });
        }
    );
    ```

2. Next up, we define the view to get all Customers and display them:

    **views/list-customers.hbs**

    ```xml
    {{!< layout}}

    <div id="customers-list" style="margin-top: 50px;"></div>

    <script>
    $(function(){
      AP.request({
          url: "/rest/api/content/",
          data: {
            "type": 'ac:my-app:customer',
            "spaceKey": "{{spaceKey}}",
            "expand": "children"
          },
          success: function(response){
            let customers = JSON.parse(response).results;
            customers.forEach(function(customer) {
              AP.request({
                url: `/rest/api/content/${customer.id}/property/customer-data`,
                success: function(jsonData) {
                  jsonData = JSON.parse(jsonData)
                  $("#customers-list").append('<div id="' + jsonData.id +'" class="company"><img src="' + jsonData.value.logo + '" alt="Logo"><div class="company-name">' + jsonData.value.companyName + '</div><div class="description">' + jsonData.value.description + '</div><div class="website">' + jsonData.value.website + '</div></div>')
                }
              });
            });
          },
          error: function(err){
            console.log("Err in fetching customers - ", error)
          }
      });
    })
    </script>
    ```

    Let's see what's happening here:
      
    - We first added the HTML div with ID `customers-list` where all the Customer details will be displayed.

    - Then, we made a GET request to **/rest/api/content/** to fetch details for our custom content type under the current Space. We have a parameter called `expand`, which specifies additional data to fetch. We specify the value `children` to retrieve all the Customers (ie. the "children") within the Space (the "parent"). However, this returns only the Customer IDs. We need a way to store and fetch the actual details of each Customer.
    
        We will store the details of the Customers in a content property, named `customer-data`. So, we fetch this content property for each Customer by making GET requests, one request per Customer, to **/rest/api/content/&lt;customer-id&gt;/property/customer-data**. This data is then rendered onto the page.

Now that we know to use content properties to store and fetch data for our custom content, let's see how to create new Customers.

## Creating a customer

Let's set up a basic dialog that looks like this:

![add a new customer](/cloud/confluence/images/image2016-7-20-17-53-17.png)

To achieve this, we need to do four things: 

1.  Setup a [dialog module](/cloud/confluence/modules/dialog/) in our descriptor. This is done by adding the following to `modules`: 

    **atlassian-connect.json**
    ``` javascript
    "dialogs": [
      {
        "key": "newCustomer",
        "url": "/add-new-customer?spaceKey={space.key}",
        "options": {
          "height": "420px",
          "width": "600px",
          "header": {
            "value": "Add a new customer"
          }
        }
      }
    ]
    ```

    Once we have set this dialog up, we can trigger it to appear from anywhere within our app. The important thing to note is the dialog's `key` attribute: `newCustomer`. We pass `spaceKey` as a URL parameter because it is required to create custom content later on.

2.  Adding a UI trigger to our list page. Specifically, we create a button that, when clicked, calls the [Dialog JS API](/cloud/confluence/jsapi/dialog/) and asks Confluence to render our dialog. Add the following to the end of the file: 

    **views/list-customers.hbs**
    ```xml
    <button id="add-customer">Add a new Customer</button>
    
    <script>
      $('#add-customer').on('click', function(){
        AP.dialog.create({
          key: 'newCustomer',
          chrome: false
        })
      });
    </script>
    ```
    Notice how we reference the dialog using the `key` attribute from step 1.

3. When the "Add a New Customer" button is clicked, the dialog box opens up and requests **/add-new-customer** as defined in step 1. Let's add that route:

    **routes/index.js**
    ```javascript
    app.get('/add-new-customer', addon.authenticate(), function (req, res) {
      var spaceKey =  req.query['spaceKey']
      res.render('new-customer', {
          spaceKey: spaceKey
      });
    });
    ```

4. Create a new file with the dialog's view:

    **views/new-customer.hbs**
    ``` xml
    {{!< layout}}

    <div>
      <div class="aui-dialog2-content">
        <form id="add-customer-dialog" class="aui top-label new-customer-dialog">
          <div class="field-group top-label">
            <label for="logo">Logo</label>
            <input class="text long-field" type="text" id="logo" name="logo" placeholder="http://path/to/a/logo" />
          </div>
          <div class="field-group top-label">
            <label for="companyName">Company Name</label>
            <input class="text long-field" type="text" id="companyName" name="companyName" placeholder="Atlassian" />
          </div>
          <div class="field-group top-label">
            <label for="description">Description</label>
            <input class="text long-field" type="text" id="description" name="description" placeholder="Collaboration tools for small and large teams" />
          </div>
          <div class="field-group top-label">
            <label for="website">Website</label>
            <input class="text long-field" type="website" id="website" name="website" placeholder="http://www.atlassian.com" />
          </div>
          <button id="dialog-submit-button" class="aui-button aui-button-primary">Create</button>
          <button id="dialog-close-button" class="aui-button aui-button-link">Close</button>
        </form>
      </div>
    </div>
    ```

To check out more markup and UI components, check out the [AUI documentation](https://docs.atlassian.com/aui/latest/).

The last step involved in creating our customer entity, is issuing a POST request to Confluence. To do this, we use the awesome AP Request library again, and send our form data through. Before doing this, let's understand the ways in which we can store the information we have acquired from our form submission.

### Storage representation

Confluence stores the XML markup of each piece of content in a native 'Storage Format'. This is sanitized HTML, which is used more for storing bodies of information or content. Typically, the content stored in Pages and Comments is in this format. It stores 'prose-like' content.

### Content properties

If we are acquiring input which is not prose-like, but rather a set of key-value pairs, we can use content properties. These are JSON objects stored against a piece of content. In our case, this would be the better approach. We capture this information under the 'metadata' key.

Awesome, now we know what our data needs to be stored as. In order to POST a new content entity, we use the [/rest/api/content](/cloud/confluence/rest/#api-content-post) endpoint. The following code generates the data in the proper format for our endpoint. Add it to the end of the file:

**views/new-customer.hbs**

``` xml
<script>
$(function(){
    $('#dialog-submit-button').on('click', function(){
      var jsonData = {
          "type": "ac:my-app:customer",
          "space": {
              "key": "{{spaceKey}}"
          },
          "title": $("#companyName").val(),
          "body": {
              "storage": {
              "value": "",
              "representation": "storage"
              }
          },
          "metadata": {
              "properties": {
                "customer-data": {
                    "key": "customer-data",
                    "value": {
                        "logo": $("#logo").val(),
                        "companyName": $("#companyName").val(),
                        "description": $("#description").val(),
                        "website": $("#website").val()
                    } 
                }
              }
          }
      }
    })
})
</script>
```

Notice how you specify content properties for a piece of custom content in `metadata.properties`. When we created the **list-customers** view earlier, we expected Customer data to be stored in the content property named `customer-data`.

We still have to take care of how `jsonData` is passed from dialog to page. This is handled by [Events](/cloud/confluence/jsapi/events/). Events are used to pass data between modules. 

When the user clicks "Submit", we want to close the dialog and emit an event with the form data:

**views/new-customer.hbs**
```javascript
// Add this inside the #dialog-submit-button click handler, after
// defining `jsonData`
AP.events.emitPublic('customerDataEvent', jsonData);
AP.dialog.close();
```

When the page module receives the event, it makes the POST request needed to create our new Customer:

**views/list-customer.hbs**
``` xml
<script>
AP.events.onPublic('customerDataEvent', function(args){
  AP.request({
      url: '/rest/api/content',
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(args),
      success: function(customer){
          customer = JSON.parse(customer);
          console.log("Customer successfully persisted to Confluence", customer);
      },
      error: function(err){
          console.log("content error - ", err);
      }
  });
});
</script>
```

You should see the following in your developer tools: 

![insert menu](/cloud/confluence/images/dev-tools-message.png)

You can now visualize the content hub using GET **/rest/api/content**, searching for the type `ac:my-app:customer`. Here's an example of what you can do with these capabilities:

![content hub](/cloud/confluence/images/image2016-7-20-17-52-21.png)

Again, the ability to handle the view for singular and all customers can be implemented in your app through the use of context parameters. Here, we are viewing the content 'hub' landing page.


Now, before we start building 'notes', let's take a look at how we can make our Customer custom content types searchable! Head over to the [Lesson 3 - Extra Searching Capabilities](/cloud/confluence/lesson-3-extra-searching-capabilities)!
