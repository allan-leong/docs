---
title: Getting started
platform: cloud
product: confcloud
category: devguide
subcategory: intro
date: "2018-08-16"
aliases:
- confcloud/quick-start-to-confluence-connect-39987884.html
- /confcloud/quick-start-to-confluence-connect-39987884.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987884
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987884
confluence_id: 39987884
---

# Getting started

This tutorial will teach you the basics of developing apps for Confluence Cloud using Atlassian 
Connect. The Atlassian Connect framework handles discovery, installation, authentication, and 
seamless integration into the Confluence user interface.

By the end of this tutorial, you'll have everything you need to start developing apps, including:

- setting up a cloud development site
- validating your setup by deploying a basic app 

## Before you begin

To complete this tutorial, you'll need a basic understanding of Confluence and the following:

- IDE or text editor
- [Node.js](https://www.nodejs.org/en/download) version 4.5.0 
- npm (bundled with Node.js)
- [Git](https://git-scm.com/downloads)

## Step 1: Cloud development site

We'll start by getting a free Confluence Cloud development site that you can use for building and 
testing apps. 

1. Go to [go.atlassian.com/cloud-dev](http://go.atlassian.com/cloud-dev) to sign up. It takes several minutes to provision your site.
1. Once your site is ready, sign in, and complete the setup wizard. 

Your cloud development site has Confluence and all of the Jira products installed, but be aware that 
Atlassian Cloud development instances have limits on the number of users.

## Step 2: Enable development mode

Development mode allows you to install app descriptors from any public URL in your development 
site. This means that you do not need to list the app in the Atlassian Marketplace before 
installing it.

1. In the left-hand navigation menu, click **Settings**.
![settings](/cloud/confluence/images/settings.png)
1. Scroll to the **Atlassian Marketplace** section, and then click **Manage apps**.
![settings](/cloud/confluence/images/manage-add-ons-setting.png)
1. Under **User-installed apps**, click **Settings**.
1. Select **Enable development mode**, and then click **Apply**.
![settings](/cloud/confluence/images/enable-development-mode.png)

After the page refreshes, you'll see the **Upload app** link. This allows you to install apps 
while you're developing them.

{{% note %}}Confluence Cloud has a new look and feel, including updated navigation. 
[Learn more about the recent changes](https://confluence.atlassian.com/confcloud/new-navigation-and-a-new-look-for-confluence-881536766.html).{{% /note %}}

## Step 3: Create a basic app

Now, we're ready to create your first app. The app is a simple [macro](https://confluence.atlassian.com/confcloud/macros-724765157.html)  
that prints *Hello World* on Confluence pages.

Now we'll clone a repository that contains the source code for the *Hello World* app. 

``` bash
git clone https://bitbucket.org/atlassian/confluence-helloworld-addon.git
```

Now that we have the source code, we're ready to get to work:

1. Switch to the app directory:

	``` bash
	cd confluence-helloworld-addon
	```
1. Install dependencies for the app using npm:

	``` bash
	npm install
	```
1. Add a **credentials.json** file in your app directory with your information:
 - **your-confluence-domain**: Use the domain of your cloud development site (e.g., *your-domain.atlassian.net*).
 - **username**: Use the email address of your [Atlassian account](https://confluence.atlassian.com/cloud/atlassian-account-for-users-873871199.html).
 - **password**: Specify the [API token](https://confluence.atlassian.com/x/Vo71Nw)

	``` json
	{
		"hosts" : {
			"<your-domain>": {
				"product" : "confluence",
				"username" : "<user@example.com>",
				"password" : "<api_token>"
			}
		}
	}
	```

1. Start the server:

	``` bash
	npm start
	```

	You should see something similar to the following: 

	``` bash
	npm start

	> helloworld-addon@0.0.1 start /home/ubuntu/workspace/confluence-helloworld-addon
	> node app.js

	Watching atlassian-connect.json for changes
	Initialized memory storage adapter
	Add-on server running at http://<your-machine-name>:8080
	Local tunnel established at https://<example>.ngrok.io/
	Check http://127.0.0.1:4040 for tunnel status
	Registering add-on...
	GET /atlassian-connect.json 200 11.418 ms - 633
	Saved tenant details for <example> to database
	{ key: 'confluence-helloworld-addon',
	  clientKey: 'example',
	  publicKey: 'example',
	  sharedSecret: 'example',
	  serverVersion: 'example',
	  pluginsVersion: '1.268.0',
	  baseUrl: 'https://<your-cloud-dev-site>.net/wiki',
	  productType: 'confluence',
	  description: 'Atlassian Confluence at null ',
	  eventType: 'installed' }
	POST /installed?user_key=example 204 49.872 ms - -
	Registered with host at https://<your-cloud-dev-site>.net/wiki
	```

Starting the service causes the app to be installed in the Confluence instance described
in the `credentials.json` file.

## Step 4: Test your app

Awesome! You've just installed your first sample app. Let's see it in action:

1. Edit any page, and type **{hello**: 
	
	![type {hello](/cloud/confluence/images/type-hello.png)

1. Select **Hello World Macro**, and save the page. You should see the following content:

	![hello world macro](/cloud/confluence/images/hello-world-macro.png)

## Next steps

Now that you have set up your cloud development site, you're ready to get started building apps. 
Check out the [other tutorials](/cloud/confluence/tutorials-and-guides), or take a look at the [REST API](/cloud/confluence/rest/) docs.
