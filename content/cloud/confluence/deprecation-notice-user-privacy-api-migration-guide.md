---
title: "REST API migration guide and deprecation notice - removal of username and userkey"
platform: cloud
product: confcloud
category: devguide
subcategory: updates
date: "2019-02-28"
---

# Deprecation notice and migration guide for major changes to Confluence Cloud REST APIs to improve user privacy

{{% note %}}
#### The deprecation period for these changes began on 01 October 2018.

By 29 April 2019, we will remove personal data from the API that is used to identify users, such as `username` and `userKey`, and instead use the Atlassian account ID (`accountId`). Additionally, we plan to restrict the visibility of other personal data, such as email, in conjunction with a user's profile privacy settings, or in the case of managed account, the visibility settings decided by the site administrator.
{{% /note %}}

This guide shows you how to update your app or integration to adopt the GDPR-related changes for the Confluence Cloud REST APIs. It describes how the user privacy improvements affect the REST APIs, provides a migration path to the new REST APIs, and lists all of the REST API changes.

## Overview

As previously announced in [Major changes to Confluence Cloud REST APIs are coming to improve user privacy](https://developer.atlassian.com/cloud/confluence/api-changes-for-user-privacy-announcement/), Atlassian is making a number of changes to our products and APIs to improve user privacy in accordance with the [European General Data Protection Regulation (GDPR)](https://www.atlassian.com/blog/announcements/atlassian-and-gdpr-our-commitment-to-data-privacy?_ga=2.227063645.1739444628.1534216821-321408603.1534216821).

For Confluence Cloud, these changes include:

- **Changes to how users are identified**: Personal data that is used to identify users, such as the `username` and `userKey`, will be removed from the REST APIs. Users will be identified by their Atlassian account ID instead.

- **Changes to the visibility of user information**: Users will be able to restrict the visibility of their personal data through their user profile privacy settings, or in the case of a managed account, the visibility settings that are decided by the site administrator. This means that fields such as email will only be returned by the API if the user has permitted that data to be visible. Note that this means that some fields can be null.

- **Changes to Atlassian Connect APIs**: All personal data will be removed from Connect REST APIs and replaces with accountId where appropriate.
For more details on how to migration connect apps to GDPR compliant version of REST APIs check [migration guide](../connect-app-migration-guide/).

The REST API changes will be introduced alongside the existing REST API. The existing REST API will be available until the end of the deprecation period. Until this time, you can use either the GDPR-compliant or non-GDPR compliant version of the REST API. An opt-in mechanism will be provided for testing, which will force Confluence Cloud to only use the GDPR-compliant version of the REST API (see *Testing GDPR changes* section).

## Summary of changes

Below is a high-level summary of the REST API changes:

- Operations that currently accept a `username` and/or `userKey` will also accept an Atlassian account ID (`accountId`). After the deprecation period ends, only an `accountId` will be accepted.

- Operations that currently return a `username` and/or `userKey` will also return an Atlassian account ID (`accountId`). After the deprecation period ends, only an `accountId` will be returned.

- Operations that currently use a `username` and/or `userKey` as a search clause (for example, JQL) will also use an Atlassian account ID. After the deprecation period ends, only an `accountId` may be used.

- The User object will be changed: Identifying personal data, such as `username` and/or `userKey`, will be removed and other personal data, such as email, will be displayed according to user settings.

To understand how to update your app or integration, read the list of REST API changes in the next section. You'll find the changes listed by resource and instructions on what you need to update.

<br>

## Content restriction

#### Resources

- /wiki/rest/api/content/{id}/restriction

#### Methods

- GET
- POST
- PUT
- DELETE

#### Request parameter changes

- For PUT and POST, the request body is shown in the example below:

``` bash
curl --request POST \
  --url 'https://your-domain.atlassian.net/wiki/rest/api/content/{id}/restriction' \
  --user 'email@example.com:' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '[
  {
    "operation": "administer",
    "restrictions": {
      "user": [
        {
          "type": "known",
          "username": "",
          "userkey": "",
          "accountId": "",
        }
      ],
      "group": [
        {
          "type": "group",
          "name": ""
        }
      ]
    }
  }
]'
```

The `username` and `userKey` fields will no longer be accepted. Use `accountId` instead.

#### Response changes

- All references to `user` objects will have changes to their fields. See [User](#user) for the full list of changes.

<br>

## Relation

#### Resources

- /wiki/rest/api/relation/{relationName}/from/{sourceType}/{sourceKey}/to/{targetType}/{targetKey}
- /wiki/rest/api/relation/{relationName}/from/{sourceType}/{sourceKey}/to/{targetType}
- /wiki/rest/api/relation/{relationName}/to/{targetType}/{targetKey}/from/{sourceType}

#### Methods

- GET
- PUT
- DELETE

#### Request parameter changes

- When `{sourceType}` is `user`, `{sourceKey}` is currently expected to be `userKey`. This will be deprecated. Instead, when `{sourceType}` is `user`, `{sourceKey}` will be expected to be `accountId`.

#### Response changes

- When `{sourceType}` or `{targetType}` is `user`, the response body will contain a user reference. See [User](#user) for the full list of changes.
- `Relationdata.createdBy` is a user reference. See [User](#user) for the full list of changes.

<br>

## Space

#### Resources

- /rest/api/space?
- /rest/api/space/_private
- /rest/api/space/:spaceKey?
- /rest/api/space/:spaceKey/content?
- /rest/api/space/:spaceKey/content/:type?

#### Methods

- GET
- PUT
- POST

#### Request parameter changes

- When creating a space via `POST /rest/api/space?`, the request body can contain references to users in the permissions section. For those users, there will be changes to the fields accepted. See the example below:

``` bash
curl --request POST \
  --url 'https://your-domain.atlassian.net/wiki/rest/api/space' \
  --user 'email@example.com:<api_token>' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{
  "key": "<string>",
  "name": "<string>",
  "description": {
    "plain": {
      "value": "<string>",
      "representation": "<string>"
    }
  },
  "permissions": [
    {
      "subjects": {
        "user": {
          "results": [
            {
              "type": "known",
              "username": "<string>",
              "userKey": "<string>",
              "accountId": "<string>",
              "profilePicture": {
                "path": "<string>",
                "width": 2154,
                "height": 2154,
                "isDefault": true
              },
              "displayName": "<string>",
              "operations": [
                {
                  "operation": "administer",
                  "targetType": "page"
                }
              ],
              "details": {},
              "personalSpace": {
                "id": 2154,
                "key": "<string>",
                "name": "<string>",
                "type": "<string>",
                "status": "<string>",
                "_expandable": {},
                "_links": {}
              },
              "_expandable": {
                "operations": "<string>",
                "details": "<string>",
                "personalSpace": "<string>"
              },
              "_links": {}
            }
          ],
          "size": 2154
        },
        "group": {
          "results": [
            {
              "type": "group",
              "name": "<string>",
              "_links": {}
            }
          ],
          "size": 2154
        },
        "_expandable": {
          "user": "<string>",
          "group": "<string>"
        }
      },
      "operation": {
        "operation": "administer",
        "targetType": "page"
      },
      "anonymousAccess": true,
      "unlicensedAccess": true
    }
  ]
}'
```

The `username` and `userKey` fields will no longer be accepted. Use `accountId` instead.

#### Response changes

- All references to `user` objects will have changes to their fields. See [User](#user) for the full list of changes.

<br>

## User

#### Resources

- /wiki/rest/api/user
- /wiki/rest/api/user/current
- /wiki/rest/api/user/anonymous
- /wiki/rest/api/user/memberof

#### Methods

- GET

#### Request parameter changes

- `username` and `userKey` are deprecated and will be removed.
- `accountId` parameter must be used to identify a user.

#### Response changes

- User object

| Field | Change | Nullable | Notes |
| ----- | ------ | -------- | ----- |
| `name` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | use `accountId` instead. |
| `key` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | use `accountId` instead. |
| `details.personal.email` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A |This field will be removed. It will be moved to the top level field `email` |
| `details.personal.im` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `details.personal.website` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `details.personal.phone` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `details.business.position` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `details.business.department` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `details.business.location` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | N/A | This field will be removed. This data will no longer be available in Confluence. |
| `accountId` | <span class="aui-lozenge aui-lozenge-success">ADDED</span> or <span class="aui-lozenge">CURRENT</span> | No | This field will be added. It is the new primary identifier for users, shared across all Atlassian systems. |
| `email` | <span class="aui-lozenge aui-lozenge-success">ADDED</span> or <span class="aui-lozenge">CURRENT</span> | Yes | new top level field, moved from `details.personal.email`. Can be null, depending on each user's privacy settings. |
| `profilePicture` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | Yes | Can be null, depending on each user's privacy settings. |
| `displayName` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | Yes | Can be null, depending on each user's privacy settings. |
| `accountType` | <span class="aui-lozenge aui-lozenge-success">ADDED</span> | No | See [Webhooks](#webhooks) below. |

## User watch

#### Resources

- /wiki/rest/api/user/watch/content/{contentId}
- /wiki/rest/api/user/watch/label/{labelName}
- /wiki/rest/api/user/watch/space/{spaceKey}

#### Methods

- GET
- POST
- DELETE

#### Request parameter changes

- `username`, `key` are deprecated and will be removed.
- `accountId` parameter must be used to identify a user.

#### Response changes

- None

## Other REST APIs

#### Request parameter changes

- No changes to the parameters of any other APIs.

#### Response changes

- All references to `user` objects will have changes to their fields. See [User](#user) for the full list of changes.


<a name="webhooks"></a>

## Webhooks

User objects in a webhook payload have a new field: `accountType`. This field is used to distinguish normal users from app users.

Valid values:

- `customer`: A regular Atlassian user account.
- `app`: A system account used for Connect applications and OAuth 2.0 to represent external systems.


## Changes to CQL

Confluence provides CQL as a method of forming complex queries to use with the search API. A number of these fields will be changing their behavior or will become deprecated:

| Field | Change | Notes |
| ----- | ------ | ----- |
| `user` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | This field currently accepts `username`. It will accept `accountId` instead. |
| `creator` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | This field currently accepts `username`. It will accept `accountId` instead. |
| `mention` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | This field currently accepts `username`. It will accept `accountId` instead. |
| `watcher` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | This field currently accepts `username`. It will accept `accountId` instead. |
| `contributor` | <span class="aui-lozenge aui-lozenge-current">CHANGED</span> | This field currently accepts `username`. It will accept `accountId` instead. |
| `user.userkey` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | Use `user.accountId` instead. |
| `creator.userkey` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | Use `creator.accountId` instead. |
| `contributor.userkey` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | Use `contributor.accountId` instead. |
| `mention.userkey` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | Use `mention.accountId` instead. |
| `watcher.userkey` | <span class="aui-lozenge aui-lozenge-error">REMOVED</span> | Use `watcher.accountId` instead. |

Furthermore, the CQL function `currentUser()` will change to return the `accountId` instead of `username`. This way, queries using the form below will continue to function correctly without any change required:

``` text
user=currentUser()
```

<br>

## Testing your GDPR changes

If you want to test your changes, you can force the REST APIs to only use GDPR-compliant functionality by setting the query parameter `privacyMode=true` in any REST API call. For example, if you get a user and set `privacyMode=true` then the response will not include a `username` and `userkey`.

You can choose to enable `privacyMode` until the end of the deprecation period, if you want to opt in early to the new GDPR-compliant REST APIs. Once the deprecation period is over, GDPR-compliant functionality will be enforced in the REST APIs, regardless of the `privacyMode` parameter.

<br>

## About the RPC and SOAP APIs

The Confluence XML-RPC, JSON-RPC and SOAP APIs have been deprecated since Confluence 5.5, which was released in 2016. See these related Confluence Server documents for more information:

- [Confluence XML-RPC and SOAP APIs](https://developer.atlassian.com/server/confluence/confluence-xml-rpc-and-soap-apis/)
- [Confluence JSON-RPC APIs](https://developer.atlassian.com/server/confluence/confluence-json-rpc-apis/)

As part of the changes we are making to our APIs to improve user privacy, these APIs will be removed completely. We will be publishing documentation describing how to migrate from these API endpoints to the REST API in the near future.
