
## JAVA

[Jira Autowatcher](https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin)  
Automatically add watchers to newly created Jira issues.

[Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview)  
Displays the users who are currently looking at a Jira issue.

## Node.js
[Webhook Inspector](https://bitbucket.org/atlassianlabs/webhook-inspector)  
Inspects the response bodies of the available webhooks in Atlassian Connect.

[Entity Property Tool](https://bitbucket.org/robertmassaioli/ep-tool)  
An Atlassian Connect app that allows manipulation of entity properties in Jira.

## Haskell
[My Reminders](https://bitbucket.org/atlassianlabs/my-reminders/overview)  
Sends you reminders about Jira issues.

## Scala

[Atlas Lingo](https://bitbucket.org/atlassianlabs/atlas-lingo)  
Translate Jira issues into other languages using Google Translate API.

[Who's Looking](https://bitbucket.org/atlassianlabs/whoslooking-connect-scala)
Displays the users who are currently looking at a Jira issue.

## Static app
Static apps consist of entirely static content (HTML, CSS, and JavaScript) and do not have a framework.

[Who's Looking](https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2)  
Displays the users who are currently looking at a Jira issue.