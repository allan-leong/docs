# 1.0-m31

Release date: 10th February, 2014

* Support for Inline Dialogs
* Apps that request JWT authentication will now fail to install if they do not specify an ``"installed"``
[lifecycle callback](https://developer.atlassian.com/static/connect/docs/latest/modules/lifecycle.html). To opt out of JWT authentication, you may specify an authentication
type of ``"none"``.
* Improvements to `"Error contacting remote application host"` error handling

#### App `version` attribute

The `version` attribute of the module descriptor should no longer be used. Instead, the `apiVersion` attribute is now
 preferred and provides a mechanism to test beta versions of your app. The `version` attribute is now ignored and
 the Atlassian Marketplace will create an internal version for it's use. Also see: [upgrades and versioning](/platform/marketplace/upgrading-and-versioning-cloud-apps/).

### Issues resolved

<table>
    <thead>
        <tr>
            <th></th>
            <th class='key'>Key</th>
            <th>Summary</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><span>Fixed</span></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-206">AC-206</a></td>
            <td>As a developer, I can create an inline dialog from a web-item location</td>
        </tr>
        <tr>
            <td><span>Fixed</span></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-926">AC-926</a></td>
            <td>AP.request is broken inside inline-dialog iframes</td>
        </tr>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-928">AC-928</a></td>
            <td>Add missing `myself` Jira REST APIs to scopes</td>
        </tr>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-924">AC-924</a></td>
            <td>Web items should use fully qualified complete key for linkId</td>
        </tr>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-934">AC-934</a></td>
            <td>Failing to specify a scope should not result in "Error contacting remote application host" error message during plugin installation</td>
        </tr>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-933">AC-933</a></td>
            <td>JSON descriptors fail on upgrade whilst looking up SEN</td>
        </tr>
        <tr>
            <td></td>
            <td><a href="https://ecosystem.atlassian.net/browse/AC-906">AC-906</a></td>
            <td>Uninstall plugin error</td>
        </tr>
    </tbody>
</table>

### Deployment timetable

1.0-m31 is scheduled for deployment on _17th February 2014_.

### Testing

You can start Jira or Confluence with Atlassian Connect as follows:

#### Jira

``` bash
atlas-run-standalone --product jira --version 6.2-OD-09-036 --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0-m31,com.atlassian.jwt:jwt-plugin:1.0-m8,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0 --jvmargs -Datlassian.upm.on.demand=true
```

#### Confluence

``` bash
atlas-run-standalone --product confluence --version 5.4-OD-5 --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.0-m31,com.atlassian.jwt:jwt-plugin:1.0-m8,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0-m0 --jvmargs -Datlassian.upm.on.demand=true
```
