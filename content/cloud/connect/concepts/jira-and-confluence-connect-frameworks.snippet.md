# Frameworks and tools

Atlassian Connect apps can be written using many different languages, frameworks and tools. Since Atlassian Connect 
apps operate remotely over HTTP and can be written with any programming language and web framework there are many
tools available for you to develop your apps.


## Atlassian frameworks

We've written two different frameworks to help you get started. These frameworks help to generate some of the plumbing
required for your Connect app, and are officially supported by Atlassian:

 * [Atlassian Connect for Node.js Express](https://bitbucket.org/atlassian/atlassian-connect-express)
 * [Atlassian Connect for Spring Boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/overview)
  
#### Additional options

Atlassian and our excellent developer community have also written a number of other frameworks that you can use. These frameworks are not supported by Atlassian but they may be supported by members of the community:

 * [Play (Java)](https://bitbucket.org/atlassian/atlassian-connect-play-java)
 * [Play (Scala)](https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala)
 * PHP
   * [Symfony2 Atlassian Connect Bundle](https://github.com/thecatontheflat/atlassian-connect-bundle)
   * [Laravel5 Atlassian Connect Core](https://github.com/brezzhnev/atlassian-connect-core)
 * Dart
   * Atlassian Connect JWT library - [atlassian_connect_jwt](https://pub.dartlang.org/packages/atlassian_connect_jwt)
   * Services for handling communications with the host product - [atlassian_connect_host](https://pub.dartlang.org/packages/atlassian_connect_host)
   * Helpers for managing environment configuration - [atlassian_connect_config](https://pub.dartlang.org/packages/atlassian_connect_config)
   * Simple web server bundling the above components - [atlassian_connect_shelf](https://pub.dartlang.org/packages/atlassian_connect_shelf)
 * Haskell
   * [atlassian-connect-core](http://hackage.haskell.org/package/atlassian-connect-core)
   * [atlassian-connect-descriptor](http://hackage.haskell.org/package/atlassian-connect-descriptor)