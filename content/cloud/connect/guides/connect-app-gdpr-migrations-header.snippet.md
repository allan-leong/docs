
# Migration guide for Connect Apps to improve user privacy

This page provides a guide for connect app developers adopting Atlassian Connect API changes to improve user privacy
in accordance with the European General Data Protection Regulation (GDPR).
These changes were announced in for [Jira](https://developer.atlassian.com/cloud/jira/platform/api-changes-for-user-privacy-announcement-connect/), [Confluence](https://developer.atlassian.com/cloud/confluence/api-changes-for-user-privacy-announcement-connect/), and [Bitbucket](https://developer.atlassian.com/cloud/bitbucket/api-changes-for-user-privacy-announcement-connect/).
As part of this GDPR work, personal data, such as *username* and *email address*, will be **removed** from various locations in our APIs.
 Where possible, we aim to provide a migration path (as covered by this document)
 to help developers move away from expecting connect to send user's personal data.

The existing APIs will be available until the end of deprecation period - *29 Mar 2019*.
During the deprecation period app developers need to make changes to their apps to *explicitly* opt-in to the changes
so that they can observe the new behavior of the API.
This guide describes the opt-in process in details below.
Where possible, to help with the transition, APIs have been extended
to accept and return extra information (most notably Atlassian account IDs) in order to facilitate
the transition to the new API without requiring explicit opt-in.
