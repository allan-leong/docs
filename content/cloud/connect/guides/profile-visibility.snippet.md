# Profile visibility

This guide helps you design and test apps to work effectively with profile visibility for users. 
Profile visibility means that some information from a user's profile may be hidden in Atlassian 
products and sites, depending on who is viewing it. By using profile visibility controls, users can 
choose to show or hide specific fields from their Atlassian account profiles, including fields such 
as name, location, and email address.

Profile visibility also affects apps. There are a number of factors that affect this, such as the type 
of app and 3LO user consent. However, in general, if the user has chosen to hide fields in their profile, 
your apps will not be able to retrieve those fields via the API.

## Profile visibility overview

The profile visibility controls for a user are found in the *Profile and visibility* tab of their 
Atlassian account profile. On this tab, the user can toggle the visibility of each field. This affects 
the information that is shown in the user's profile in the Atlassian people directory, Atlassian products 
(for example, Jira Software), and Atlassian public forums (for example, Atlassian Community). To learn 
more, see [Update your profile and visibility settings](https://confluence.atlassian.com/x/6IS8OQ).

### Default settings

The default settings for profile visibility are different depending on whether the user account is a 
managed or unmanaged account. The default settings for each type of account are shown below.

#### Default profile visibility settings for managed accounts

Managed accounts are accounts with email addresses that belong to a domain that has been verified. 
To learn more, see [Managed accounts](https://confluence.atlassian.com/x/YzcWN). Profile visibility 
controls for managed accounts have an additional level of visibility, _Organization_, which represents 
the people that the user works with. The default settings for managed accounts are shown below. 

| Profile field | "Anyone" (Public) | Organization | "Only you" (Private) |
|---|---|---|---|
| Avatar | `hide` | `show` | `show` |
| Full name | `show` | `show` | `show` |
| Job title | `show` | `show` | `show` |
| Department | `hide` | `show` | `show` |
| Organization | `hide` | `show` | `show` |
| Location | `hide` | `show` | `show` |
| Timezone | `hide` | `show` | `show` |
| Email address | `hide` | `hide` | `show` |

Notes:

* _Avatar_ shows a masked avatar (the user's initials) rather than the profile picture, if it is 
restricted.
* Every user has a _Public name_ that cannot be restricted. If a user restricts their _Full name_, 
then the _Public name_ is displayed to people outside the organization. _Public name_ is also displayed 
on Atlassian public forums (for example, Atlassian Community, jira.atlassian.com). 
* The `displayName` property for user objects stores the name that is currently displayed in Atlassian 
products (either _Full name_ or _Public name_).
* _Job title_, _Department_, and _Organization_ are part of the profile visibility controls in Jira 
and Confluence. However, these fields are not returned by the Jira and Confluence REST APIs. They are 
included in the table above, just to show that that are part of the profile visibility settings UI.

#### Default profile visibility settings for unmanaged accounts

Unmanaged accounts are accounts that are not managed accounts. That is, they do not have emails that 
belong to a domain that has been verified.

| Profile field | "Anyone" (Public) | "Only you" (Private) |
|---|---|---|
| Avatar | `show` | `show` |
| Full name | `show` | `show` |
| Job title | `show` | `show` |
| Department | `show` | `show` |
| Organization | `show` | `show` |
| Location | `show` | `show` |
| Timezone | `show` | `show` |
| Email address | `hide` | `show` |

Notes:

* _Avatar_ shows a masked avatar (the user's initials) rather than the profile picture, if it is restricted.
* _Full name_ is always visible to the public ("Anyone") for unmanaged accounts. Every user also has 
a _Public name_, which cannot be restricted and is displayed on Atlassian public forums (for example, 
Atlassian Community, jira.atlassian.com).
* The `displayName` property for user objects stores the _Full name_, which is always the display name 
in Atlassian products.
* _Job title_, _Department_, and _Organization_ are part of the profile visibility controls in Jira 
and Confluence. However, these fields are not returned by the Jira and Confluence REST APIs. They are 
included in the table above, just to show that that are part of the profile visibility settings UI.

### Profile visibility and third-party apps

Apps access profile information through the public cloud REST APIs. Profile visibility can affect the 
profile information that apps can access, depending on the type of app.

#### Connect apps

Connect apps only have access to profile information that is available to the public ("Anyone"), with 
the exception of the following fields:

* Timezone: The timezone can be accessed using [AP.user.getTimeZone](https://developer.atlassian.com/cloud/jira/platform/jsapi/user/) 
in the JavaScript API, regardless of profile visibility settings. However, you must only use this API 
to establish context for the user. Do not present the user's timezone in a way that can be seen by other 
users viewing the application.
* Email address: Profile visibility settings restrict access to the email address, similar to the other 
profile fields. However, given that some apps need email addresses for key functionality, we provide 
an _Email API_ that provides access to email addresses regardless of profile visibility settings. The 
_Email API_ is a public API but only apps that have been approved and whitelisted are permitted to use 
it. To request access, see [Requesting access to the Email API](#requesting-access-to-the-email-api) below.

#### 3LO apps

_Note that this section does not apply to Bitbucket Cloud, as Bitbucket Cloud does not have 3LO apps._

[3LO apps](../oauth-2-authorization-code-grants-3lo-for-apps/) have access to _all_ profile information 
for users that provide explicit consent through the 3LO consent flow. This is regardless of the profile 
visibility settings for the consenting users. If the user revokes their consent, then the app will no l
onger have access to the user's profile information. 

* Email address: Given that some apps need email addresses for key functionality, we provide an _Email API_ 
that provides access to email addresses regardless of user consent. The _Email API_ is a public API 
but only apps that have been approved and whitelisted are permitted to use it. To request access, 
see [Requesting access to the Email API](#requesting-access-to-the-email-api) below.

## Designing for profile visibility

Given that profile visibility can affect the user information available to your apps, it's important 
to design your app to account for this.

* **Differentiating users in the UI:** Consider using the combination of display name and avatar 
(profile picture) to identify users throughout the UI. Both of these fields are always available. 
Using both fields helps differentiate users, particularly if there are users with similar names or avatars.
* **Never use email address in the UI:** Do not use _Email address_ to help differentiate users in 
the UI. By default, _Email address_ is hidden for all account types, so your app is unlikely to have 
access to it. <br>If your app does have unrestricted access to _Email address_ (via the _Email API_), 
you should still avoid exposing email addresses in the UI. Even though your app has been approved to 
access the _Email address_, other installed apps or users may not have permission to view user email 
addresses.
* **Never use Atlassian account ID in the UI:** While the Atlassian account ID of a user is not 
restricted by profile visibility settings, do not use it to identify users in the UI. Atlassian 
account IDs are intentionally opaque and are difficult to read.
* **Displaying stored JQL queries (Jira only):** If your app displays JQL, use the following AtlasKit 
component. This component swaps the user's `accountId` with their current `displayName` when displaying JQL. 
    * _AtlasKit component is under development. For details, 
    see [ACJIRA-1710](https://ecosystem.atlassian.net/browse/ACJIRA-1710)._

Finally, be aware of your personal data reporting obligations. If your app is storing personal data, 
it must report the stored account IDs every 15 days. For details, see the 
[User privacy guide for app developers](../user-privacy-developer-guide/).

## Testing for profile visibility

The following section provides guidelines for testing profile usability settings with your app. Before 
you begin, you'll need the following:

* A developer instance to install your app in. If you don't have a developer instance, get one here 
for free: http://go.atlassian.com/about-cloud-dev-instance
* At least one additional user (other than yourself) in your developer instance, so that you can test 
the scenarios in the [Test your app](#test-your-app) section below. Note that you can add up to five 
users in a free developer instance, but be aware that an app user counts as one of these users.

### Install your app

Follow the steps below to install your app in your developer instance:

1. In your developer instance, navigate to the in-product Marketplace:
	- Jira: Go to **Jira Settings** > **Apps** > **Manage apps** > **Upload app**.
	- Confluence: Go to **Settings** > **Manage apps** > **Upload app**.
	- Bitbucket Cloud: Go to profile pic > **App marketplace** > **Installed apps** > **Install app from URL**.
1. Enter the URL for your app and install it.

Note: For Jira and Confluence, if the **Upload app** link is missing, click **Settings** on the _Manage 
apps_ screen and ensure that **Enable development mode** is checked.

### Test your app

This section describes the profile visibility scenarios you should test for your app. It also provides 
examples of what the user objects should look like for different scenarios.

#### Test the default profile visibility settings

To test the default profile visibility settings, you just need to query a user using the REST API 
(for example, [Get user](https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-user-get) 
for Jira, [Get user](https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-user-get) 
for Confluence), or [Get user](https://developer.atlassian.com/bitbucket/api/2/reference/resource/user) 
for Bitbucket Cloud. See the Jira and Confluence example responses below.

Test the following scenarios for default profile visibility settings:

* Viewing yourself
* Viewing another user
* Viewing an app user (`accountType` is set to `app`)

Note that there are other types of users that you should also be aware of. These are generally not 
available for testing:

* Customers: The `accountType` property is set to `customer` for customers. A customer is a Jira 
Service Desk user without an Atlassian account.
* Deactivated users: The `active` property is set to `inactive` for deactivated users. This type of 
user is inactive but we haven’t deleted their personal information yet. This user does not have access 
to Atlassian systems and therefore can’t be assigned or "at" mentioned.
* Deleted users: The `active` property is set to `inactive` for deleted users, similar to deactivated 
users. However, for deleted users, we've processed data deletion already (groups and roles that the 
deleted user belonged to are no longer associated with that accountID). These users also can’t be 
assigned or "at" mentioned. If you want to test the deletion process, you should use a test account 
since data deletion is irreversible. Data deletion is processed 14 days after the request to allow 
time for the user to change their mind.

##### Jira examples

For Jira, fields that are hidden are not included in the returned user object. Also, the user object 
does not have a property for public name, instead the `displayName` can contain either the full name 
or public name.

Default settings for a user with a personal account:
``` json
{
	self: "...",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	avatarUrls: {
	        16x16: "will always be provided containing either an identifying avatar or a masked version"
	        24x24: "Same as above"
	        32x32: "Same as above"
	        48x48: "Same as above"
	},
	displayName: "string value will always be provided containing either a full name or the user's public name",
	active: true,
	timeZone: "will provide the user's timezone ID (e.g. America / Los Angeles)",
	locale: "will provide the user's locale language tag (e.g. en-US)",
	groups: {
	      size: 3,
	      items: []
	},
	applicationRoles: {
	     size: 3,
	     items: []
	},
	accountType: "atlassian",
	expand: "groups, applicationRoles"
}
```

Default settings for a user with a managed account:
``` json
{
	self: "...",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	avatarUrls: {
	        16x16: "will always be provided containing either an identifying avatar or a masked version"
	        24x24: "Same as above"
	        32x32: "Same as above"
	        48x48: "Same as above"
	},
	displayName: "string value will always be provided containing either a full name or the user's public name",
	active: true,
	groups: {
	      size: 3,
	      items: []
	},
	applicationRoles: {
	     size: 3,
	     items: []
	},
	accountType: "atlassian",
	expand: "groups, applicationRoles"
}
```

Default settings for a user with a deleted account:
``` json
{
	self: "...",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	avatarUrls: {
	        16x16: "will always be provided containing either an identifying avatar or a masked version"
	        24x24: "Same as above"
	        32x32: "Same as above"
	        48x48: "Same as above"
	},
	displayName: "string value will always be provided containing the user's last known public name or "Former user" ",
	active: false,
	groups: {
	      size: 0,
	      items: []
	},
	applicationRoles: {
	     size: 0,
	     items: []
	},
	accountType: "atlassian",
	expand: "groups, applicationRoles"
}
```

##### Confluence examples

For Confluence, fields that are hidden are set to an empty string in the returned user object. Also, 
the user object has a property for public name (`publicName`). If the user chooses to hide their full 
name, `displayName` is set to the same value as `publicName`.

Default settings for a user with a personal account:
``` json
{
	type: "known",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	accountType: "atlassian", 
	email: "",
	publicName: "string value will always be provided containing the user's public name",
	zoneInfo: "Will provide the user's timezone ID (e.g. America / Los Angeles)",
	locale: "Will provide the user's locale language tag (e.g. en-US)",
	active: true,
	profilePicture: {
	        path: "will always be provided containing either an identifying avatar or a masked version"
	        width: 48,
	        height: 48,
	        isDefault: true
	},
	displayName: "string value will always be provided containing either a full name or the user's public name",
	_expandable: {
	       operations:"...",
	       details:"...",
	       personalSpace:"...",
	},
	_links: {
	      self: "...",
	      base:"...",
	      context: "...",
	}
}
```

{{% note %}}_Note that the `zoneInfo` property may eventually be renamed to `timeZone` to align with 
the naming conventions in the Jira API._{{% /note %}}

Default settings for a user with a managed account:
``` json
{
	type: "known",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	accountType: "atlassian", 
	email: "",
	publicName: "string value will always be provided containing the user's public name",
	zoneInfo: "",
	locale: "",
	active: true,
	profilePicture: {
	        path: "string value will contain a masked version of the avatar"
	        width: 48,
	        height: 48,
	        isDefault: true
	},
	displayName: "string value will always be provided containing the user's public name",
	_expandable: {
	       operations:"...",
	       details:"...",
	       personalSpace:"...",
	},
	_links: {
	      self: "...",
	      base:"...",
	      context: "...",
	}
}
```

Default settings for a user with a deleted account:
``` json
{
	type: "known",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	accountType: "atlassian", 
	email: "",
	publicName: "unknown",
	zoneInfo: "",
	locale: "",
	active: false,
	profilePicture: {
	        path: "string value will contain a URL to a default anonymous avatar."
	        width: 48,
	        height: 48,
	        isDefault: true
	},
	displayName: "string value will always be provided containing either user's last known public name or "Former User" ",  
	_expandable: {
	       operations:"...",
	       details:"...",
	       personalSpace:"...",
	},
	_links: {
	      self: "...",
	      base:"...",
	      context: "...",
	}
}
```

### Test changes to the profile visibility settings

Once you have tested the default settings, make changes to the profile visibility settings for users 
in the UI and check the responses from the API. 

For example, modify your own Atlassian account profile in the UI then query it using the REST API. Fields that you’ve set to _Only you_ or _Organization_ will not be included in the returned user object. Alternatively, if you change the visibility of any fields to public, these fields will included in the returned user object. 

Jira and Confluence example responses are shown below.

##### Jira examples

All fields are unhidden:
``` json
{
	self: "string",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	emailAddress: "will provide the user's email address"
	avatarUrls: {
	        16x16: "string will always be provided containing either a URL to either an identifying avatar or a masked version"
	        24x24: "Same as above"
	        32x32: "Same as above"
	        48x48: "Same as above"
	displayName: "string value will always be provided containing either a full name or the user's public name",
	active: true,
	timeZone: "will provide the user's timezone ID (e.g. America / Los Angeles)",
	locale: "will provide the user's locale languageID (e.g. en-US)",
	groups: {
	      size: 3,
	      items: []
	},
	applicationRoles: {
	     size: 3,
	     items: []
	},
	accountType: "atlassian",
	expand: "groups, applicationRoles"
}
```

All fields are hidden:
``` json
{
	self: "string",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	avatarUrls: {
	        16x16: "will always be provided containing either an identifying avatar or a masked version"
	        24x24: "Same as above"
	        32x32: "Same as above"
	        48x48: "Same as above"
	displayName: "string value will always be provided containing either a full name or the user's public name",
	active: true,
	groups: {
	      size: 3,
	      items: []
	},
	applicationRoles: {
	     size: 3,
	     items: []
	},
	accountType: "atlassian",
	expand: "groups, applicationRoles"
}
```

##### Confluence examples

All fields are unhidden:
``` json
{
	type: "known",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	accountType: "atlassian", 
	email: "will provide the user's email address",
	publicName: "string value will always be provided containing the user's public name",
	zoneInfo: "will provide the user's timezone ID (e.g. America / Los Angeles)",
	locale: "will provide the user's locale language tag (e.g. en-US)",
	active: true,
	profilePicture: {
	        path: "will always be provided containing either an identifying avatar or a masked version"
	        width: 48,
	        height: 48,
	        isDefault: true
	},
	displayName: "string value will always be provided containing either a full name or the user's public name",
	_expandable: {
	       operations:"",
	       details:"",
	       personalSpace:"",
	},
	_links: {
	      self: "string",
	      base:"string",
	      context: "string",
	}
}
```

All fields are hidden:
``` json
{
	type: "known",
	accountId: "1-128 character string - may contain "-" or ":" characters",
	accountType: "atlassian", 
	email: "",
	publicName: "string value will always be provided containing the user's public name",
	zoneInfo: "",
	locale: "",
	active: true,
	profilePicture: {
	        path: "will always be provided containing either an identifying avatar or a masked version"
	        width: 48,
	        height: 48,
	        isDefault: true
	},
	displayName: "string value will always be provided containing either a full name or the user's public name",
	_expandable: {
	       operations:"...",
	       details:"...",
	       personalSpace:"...",
	},
	_links: {
	      self: "...",
	      base:"...",
	      context: "...",
	}
}
```

## Requesting access to the Email API

The Email API provides approved apps with access to the email addresses of users. An approved app 
has access for as long as it is installed, regardless of user profile visibility settings or 
consent provided via 3LO. 

Note that the Email API is a public API that is documented and supported by Atlassian. However, 
only approved apps can use it.

### Requirements

Before you apply, ensure that your app meets the following criteria for access to the Email API.

Your app must meet the requirements for listing on the Atlassian Marketplace (even if the app 
will not be listed on the Atlassian Marketplace):
   - Provide a privacy policy.
   - Provide a customer terms of use agreement.
   - Indicate whether or not the app collects and stores personal data.

Also, your app must only use user email addresses in an appropriate manner. For example, sending 
security alerts, but not sending marketing emails. The next section, about applying for access, lists 
the valid and invalid use cases for using email addresses.


### Applying for access

You must submit a request for each app that you want to access the Email API. To request access for 
an app:

1. Create a [Request access to the Email API](https://ecosystem.atlassian.net/servicedesk/customer/portal/14/create/284) 
ticket.
1. Fill out the fields as required, including your app's use cases for emails. Be aware that certain 
use cases are generally considered to be valid uses of emails and are more likely to be approved. 
Other use cases are not valid and won't be approved:
   - Valid use cases:

   | Process using email | Functionality provided |
   |---|---|
   | Send transactional emails | Send error reports |
   |   | Send security alerts |
   |   | Allow users to subscribe to alerts / news feeds |
   |   | Allow users to invite other users to collaborate |
   | Account linking | Connect a mail service for inbound / outbound mail processing |
   |   | Sync users with another system |

   - Invalid use cases:

   | Process using email | Functionality provided |
   |---|---|
   | Send transactional emails | Inviting users to company events |
   |   | Send newsletters, campaigns, or other marketing emails |
   |   | Send materials to facilitate end user onboarding |
   |   | Send subscription / renewal reminders |
1. Submit the request. Some requests are auto-approved after submitting the form. Other requests 
need further review. 

Once your app is approved, a few things occur:
- You're notified that your app has been added to a whitelist to access the Email API.
- Your app's listing on Atlassian Marketplace is updated. The listing will show customers that the app 
is approved to access email addresses, regardless of profile visibility control settings.
- When your app is installed, it displays a new scope associated with the Email API.

Your app can now use the Email API. However, be aware of the guidelines governing the use of email 
addresses described below.

### Guidelines for using email addresses

The following guidelines govern the use of email addresses by your app:

- Only use email addresses for the functionality described in your request. If you need to use email 
addresses for other use cases, (for example, new features), inform us by either raising a new request 
or updating your existing request.
- Do not use email addresses in the UI, as described in [Designing for profile visibility](#designing-for-profile-visibility). 
- Do not share email addresses with any other third parties (including other apps) unless they are required 
to provide the service to the end user.

By accessing the Email API you also agree to our 
[Atlassian Developer Terms](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/?utm_source=%2Fmarket%2Fatlassian-developer-terms&utm_medium=302) 
and 
[Atlassian Marketplace Vendor Agreement](https://www.atlassian.com/licensing/marketplace/publisheragreement), 
which reserves Atlassian the right to conduct audits at any time to confirm your compliance with these 
Guidelines and any related procedures. We also reserve the right to remove your app at any time from 
the Email API whitelist which will prevent your app from receiving further access to email address, 
and if necessary, to de-list your app from Atlassian Marketplace.

## Support

* If you've found a bug, report it in our [Developer Service Desk](https://ecosystem.atlassian.net/servicedesk/customer/portal/14).
* If you have questions, ask in our [Developer Community](https://community.developer.atlassian.com/). 
Alternatively, ask our Developer Support team by raising a request in our 
[Developer Service Desk](https://ecosystem.atlassian.net/servicedesk/customer/portal/14).
