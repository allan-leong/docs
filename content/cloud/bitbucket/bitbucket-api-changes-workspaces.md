---
title: "Change notice: Updates to Bitbucket Cloud repository URL paths"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2019-04-08"
---

# Change notice: Updates to Bitbucket Cloud repository URL paths

As [announced previously](https://developer.atlassian.com/cloud/bitbucket/bitbucket-api-changes-gdpr/), Bitbucket Cloud REST APIs are changing to improve user privacy. This page guides you through the process of reliably generating repository URL paths in your integrations. As the previous notice stated,

> We are excluding repository URLs from this deprecation to avoid breaking repository references. As part of giving users better control of their data, we are going to separate references to user-spaces from content-spaces, which will preserve existing repository URLs.

Users are able to separately define a username and workspace ID. The username is used to authenticate over SSH or HTTPS using the Git command line client. The workspace ID is used to form repository URL paths.

## Calls to the REST API

You should review your integration’s code to determine if assumptions have been made that repository URLs are of the form `{repo.owner.username}/{repo.slug}`. If your code works this way it will continue to operate correctly for users whose username and workspace ID match. This is the default case, and will be the majority of users. However, it is more correct to form repository URLs in the following manners. Furthermore the repository owner’s username will no longer be included in Bitbucket API responses after April 29.

### Use the links section of API responses

If available, use the links section of API responses to follow up with requests to other API endpoints nested under a repository. This is good general advice for future-proofing your code and should always be your preferred method. API responses contain a links section like the following example where a user has set their username to myusername, but their workspace ID to myworkspaceid.

``` shell
GET https://api.bitbucket.org/2.0/users/557058:c0b72ad0-1cb5-4018-9cdc-0cde8492c443/

{
    "links": {
        "repositories": {
            "href": "https://api.bitbucket.org/2.0/repositories/myworkspaceid"
        },
```

### Use full_name field from the repositories returned in API responses

While using the links is preferred, you may be maintaining code that builds repository URL paths in the following manner

``` shell
# *** Old code ***
repo_url = 'mypath/' + repo.owner.username + '/' + repo.slug
```

Replace this code using the `full_name` field from the repository representation in API responses.

``` shell
# *** New code ***
repo_url = 'mypath/' + repo.full_name
```

## Atlassian Connect apps

For developers of an [Atlassian Connect app](https://developer.atlassian.com/cloud/bitbucket/integrating-with-bitbucket-cloud/) that integrates in the Bitbucket Cloud UI, there is another consideration. If your app uses the [API proxy module](https://developer.atlassian.com/cloud/bitbucket/proxy-module/), take a look at your URL templates. URL templates in the following form

``` shell
"/proxy-example/{target_user}/{repository}": {
```

should be changed if your app is passing in the username for `{target_user}`. You have two options for replacing this functionality. The first is the preferred method, but you may find the second more practical if you are maintaining a complex legacy code base.

### Use repository UUID

Change your URL template to

``` shell
"/proxy-example/{repository}": {
```

and change your code to pass in the repository UUID. `target_user` will still be available for expansion in the destination URL template even though it is not included in the inbound URL.

### Use the workspace ID

Change your URL template to

``` shell
"/proxy-example/{workspace}/{repository}": {
```

and change your code to pass in the workspace ID. You can make calls to this endpoint using the `full_name` field from repositories returned by Bitbucket API endpoints. Again, `target_user` will be available for expansion in the destination URL template even though it is not included in the inbound URL.
