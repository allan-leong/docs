---
title: "App context"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: blocks
date: "2015-12-02"
---

# App context 

App context determines the level at which an app is visible and active once a user installs the app. 

* `account` (default): the account context limits the app to the account or team which installed the app. In this case the app will be visible to anyone with access to the repository, account, or module containing the app.  

* `personal`: only the user who installs the app will be able to see the app, and it will "follow" the user around the site. 

You can see the account context applied at the bottom of our example app descriptor. This app context is why the example app only installs in your account and is visible to only those who have access to the account or team which installed the app.

``` javascript
{
    "key": "example-app",
    "name": "Example App",
    "description": "An example app for Bitbucket",
    "vendor": {
        ...
    },
    "baseUrl": {
        ...
    },
    "lifecycle": {
       ...
    },
    "modules": {
        ...    
    },
    "scopes": [...],
    "contexts": ["account"]
}
```
