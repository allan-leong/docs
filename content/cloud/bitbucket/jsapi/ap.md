---
title: "AP"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# AP

Utility methods that are available without requiring additional modules.

## Methods
    
### getLocation (callback)

Get the location of the host page.
    
#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>

	<tbody>
        <tr>
                <td><code>callback</code></td>
            <td>function</td>
            <td>function (location) {...}</td>
        </tr>
	</tbody>
</table>

    
#### Example
        
``` javascript
AP.getLocation(function(location){
    alert(location); 
}); 
```

### resize (width, height)

Resize this frame.
    
#### Parameters
        
<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>

        <tr>
                <td><code>width</code></td>

            <td>String</td>

            <td>the desired width</td>
        </tr>
        <tr>
                <td><code>height</code></td>
            <td>String</td>

            <td><p>the desired height</p></td>
        </tr>

	</tbody>
</table>
