---
title: "Showcase eazyBI"
logo_url: /img/logo-eazybi-white.png
logo_css_class: eazybi-logo
heading1: "The road to success"
byline: "eazyBI scales their easy-to-use business intelligence application with a Marketplace app" 
lede: "eazyBI is a Latvia based company with an easy-to-use app for powerful visualization and project analysis in Jira. Raimonds Simanovskis, CEO and founder of eazyBI, never wanted the company to be a typical startup. Even without venture capital, an office, or any high growth plans, eazyBI managed to build a company from zero to $3 million in less than seven years and create one of the most popular business intelligence apps for Jira."
---
## Start small; stay lean

*“I started my company all by myself in March 2011 and was working alone for about two years. I had some savings, no mortgage, I did not want to risk too much – I just wanted to explore different ideas,” says Raimonds Simanovskis.*

Prior to [eazyBI](https://eazybi.com/), Raimonds worked on software development and implementation projects for more than 15 years. He had been using and implementing business intelligence products by "big vendors," and liked the value they offered customers. However, those products were too time-consuming and delayed the insights that they could provide. This challenge lead him to come up with the idea to build an easy-to-use business intelligence application with standard data source integrations that could provide value faster. After a few initial experiments, Raimonds decided to work on it full time and founded the company eazyBI. Within a couple of months, the first beta version was launched, and half a year later the company had its first pilot customers.

Raimonds kept the company small and only decided to hire his first employee in the third year. In the fifth year, they expanded to a company of six. Now, seven years later, there are 18 people working at eazyBI and Raimonds still prefers to keep the team lean. He believes that keeping the team small allows everyone to work together more efficiently.

## Remote setup

The entire eazyBI team works 100% remotely. After working in corporate business and spending hours in traffic every day, Raimonds decided that having an office was not necessary. Why should they have an office in one location when they serve a global market? The team could work equally well or better from any place with a good internet connection. Each eazyBI employee is set up with their own “home office,” and the team is notorious for taking advantage of the summer weather and working outside. With different collaboration solutions like Slack, Trello, Zoom, and Jira, it’s easy for the team to communicate regularly while working remotely. 

Additionally, the team also organizes monthly face-to-face meetings where they review business results, past and upcoming events, and can catch up with each other over a few beers. As part of those meetings, they hold internal hackathons where they brainstorm on various challenges - software, marketing, or company related.

## All-hands-on-support

At eazyBI, the team has an “all-hands-on-support” policy. This unique approach enables them
to better understand the customer challenges and needs. Everyone from marketing, operations, and even their developers are involved in customer support. Every two weeks they rotate and two of them become full-time tech-support members. The approach allows the team to get direct feedback from their customers on a regular basis and find workarounds when users ask for functionality that eazyBI does not provide. This process leads to much better ideas and helps them develop useful new features, which eventually benefit all customers.

## Road to success

At first, eazyBI’s software was created as a cloud integration with other web applications (Basecamp, Highrise and later excel, google sheets, and Oracle). But Raimonds had worked with Jira for a long time in his previous company and often used reporting tools on top of Jira and saw a good business opportunity for eazyBI. Tired of the continuous paperwork that came with the direct sales, Raimonds was looking to outsource as much of the sales process as possible. The Atlassian Marketplace attracted eazyBI because it provided visibility to a large customer base, automated the sales process, and took away the pain of becoming approved vendors for customers.

In the fall of 2012, eazyBI’s cloud app for Jira was published on [Atlassian Marketplace](https://marketplace.atlassian.com/) and they started seeing early success when happy customers started purchasing the app at the end of their free trials. In the first month after their launch, eazyBI had earned $9k in-app sales—a trend that would continue netting them over $150,000 in their first year of listing on the Atlassian Marketplace. With pricing aligned to that of similar Jira apps in the Marketplace, it has remained largely constant to what it is today, with only a few adjustments made over time for the app’s larger user tiers. 

The [eazyBI app for Jira](https://marketplace.atlassian.com/apps/1211508/eazybi-reports-and-charts-for-jira-cloud?hosting=cloud&tab=overview) was originally built on a different technology stack than typical Jira apps. Built using the Ruby programming language (using the JRuby implementation on top of JVM) and the Ruby on Rails framework, the eazyBI app was initially built for Jira Cloud using Jira REST APIs to create custom Jira reports and customized calculations. Shortly after the launch, Atlassian customers who run Jira behind their firewall started asking for an installable version so the team began building the eazyBI app for Jira Server. At first, this proved to be an interesting technical challenge to wrap their existing technology stack inside a Jira Server app (which requires Java). While difficult at first, using the open source frameworks, eazyBI was able to adjust to their specific needs and ship their server app. 

> <img class="quote-profile" src="/img/raimonds-quote.png"> "One thing I advise to anyone is to pick an idea that you know and love to spend time on. It might take a long time to grow the business and make it profitable and even if it will fail you will not regret it if you liked what you were doing."

Even with other standalone versions and several web integrations, the Atlassian ecosystem has become eazyBI’s most successful sales channel. As eazyBI’s Reports and Charts for Jira app slowly became more and more popular, Atlassian Solution Partners started to pay attention. In a few years, eazyBI built up more than 40 relationships with different Atlassian Solution Partners to help the partners learn, market and sell eazyBI’s app. The additional effort of attending Solution Partner events and providing extensive support to partner’s customer projects has largely paid off. Now, a majority of eazyBI sales come through Solution Partners and the team plans to continue to leverage this marketing and sales channels by growing their partner network. 

## What's next

Now recognized as an Atlassian Platinum Top Vendor, the company has seen a 442% revenue growth over the last 3 years and is also the first Latvian company in the last decade to be ranked by [Deloitte as one of the top 50 fastest growing technology companies in Central Europe](https://www2.deloitte.com/content/dam/Deloitte/ce/Documents/about-deloitte/ce-technology-fast50-ranking-list-2018.pdf). 

But eazyBI doesn’t plan to stop there. In addition to improving eazyBI to make it easier for standard use cases, they are working on new data sources and integrations with both Atlassian products as well as with other Marketplace vendor products. Their slogan is "simple things are easy, hard things are possible" and they strive to continue to make hard things easier for customers.

{{% linkblock href="../adaptavist/" img="/img/clouds.png" text="Adaptavist" section="cloud" align="left" %}}
{{% linkblock href="../wittified/" img="/img/chat-bubble.png" text="Wittified" section="cloud" align="right" %}}

