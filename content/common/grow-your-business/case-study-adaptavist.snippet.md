<h2 class="title">Adaptavist</h2>

“If you’ve written something useful to you, which is how most apps start, it’s going to be useful for other people. You’re certainly not the only person with the problem that it’s solving,” says Jamie Echlin, creator of ScriptRunner.

<a href="/showcase/adaptavist/">Learn more<i class="fa fa-arrow-right" aria-hidden="true"></i></a>